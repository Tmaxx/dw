-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: 127.0.0.1    Database: dwtim
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand` (
  `id_brand` int NOT NULL AUTO_INCREMENT,
  `brand` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
INSERT INTO `brand` VALUES (1,'Adidas'),(2,'Nike'),(3,'Reebok');
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id_category` int NOT NULL AUTO_INCREMENT,
  `category` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'кроссовки'),(2,'кеды');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `color` (
  `id_color` int NOT NULL AUTO_INCREMENT,
  `color` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_color`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'черный'),(2,'белый'),(3,'красный'),(4,'желтый'),(5,'зеленый'),(6,'синий'),(7,'голубой'),(8,'оранжевый'),(9,'розовый'),(10,'фиолетовый'),(11,'серый');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` int NOT NULL,
  `brand` int NOT NULL,
  `model` varchar(200) NOT NULL,
  `img` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `color` int NOT NULL,
  `season` int NOT NULL,
  `material` varchar(100) NOT NULL,
  `price` int NOT NULL,
  `order_count` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

LOCK TABLES `goods` WRITE;
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (1,1,3,'Classics Zig Kinetica ','1.jpg','Кроссовки Reebok Classics Zig Kinetica - одна из самых футуристичных моделей бренда. Верх модели выполнен из плотного текстиля, который обладает рельефным строением. Использование этого материала придает универсальность в использовании и простоту в уходе. Система шнуровки лаконично вписывается в образ модели. Внутри модели установлены мягкие вставки, которые обеспечивают дополнительный комфорт во время ходьбы. Промежуточный материал подошвы выполнен по новейшим технологиям изготовления, что позволяет добиться максимальной амортизации. Резиновая подметка способна взаимодействовать с разными поверхностями. По периметру модели имеются фирменные логотипы бренда.',1,3,'Кожа, текстиль',6390,1),(2,1,2,'Air Barrage Low','2.jpg','Кроссовки Nike Air Barrage Low - низкая модель, отлично подходящая для повседневной носки. В строении верха используется замша и нейлоновая сетка. Сочетание этих материалов позволяет добиться максимальной универсальности. Система шнуровки лаконично вписывается в образ модели. В области пятки установлена специальная петля, которая обеспечивает удобство во время надевания. Промежуточный материал подошвы изготавливается с использованием новейших технологий. Благодаря этому достигается максимальная амортизация. Резиновая подметка обладает уникальным рисунком, с помощью которого обеспечивается взаимодействие с разными поверхностями. ',5,3,'Замша, текстиль',6390,2),(3,1,2,'MX-720-818','3.jpg','Кроссовки Nike MX-720-818 выполнены с использованием влагоотталкивающих материалов. Несмотря на это, они прекрасно дышат не вызывая дискомфорта. Система шнуровки лаконично вписывается в образ модели. В области пятки и на язычке имеется специальная петля, которая обеспечивает дополнительный комфорт при надевании. Внутри модели установлены дышащие стельки. Промежуточный материал подошвы состоит из воздушного баллона, который отлично амортизирует любые нагрузки. Резиновая подметка способна взаимодействовать с разными поверхностями. На боковой части подошвы имеет надпись с номером модели.',9,3,'Текстиль',6390,1),(4,1,3,'Classics Zig Kinetica','4.jpg','Кроссовки Reebok Classics Zig Kinetica - одна из самых футуристичных моделей бренда. Верх модели выполнен из плотного текстиля, который обладает рельефным строением. Использование этого материала придает универсальность в использовании и простоту в уходе. Система шнуровки лаконично вписывается в образ модели. Внутри модели установлены мягкие вставки, которые обеспечивают дополнительный комфорт во время ходьбы. Промежуточный материал подошвы выполнен по новейшим технологиям изготовления, что позволяет добиться максимальной амортизации. Резиновая подметка способна взаимодействовать с разными поверхностями. По периметру модели имеются фирменные логотипы бренда.',2,3,'Текстиль ',6390,3),(5,1,2,'Lebron XVIII ','5.jpg','Баскетбольные кроссовки Nike LeBron XVIII - именная модель знаменитого баскетболиста NBA Леброна Джеймса. Пара изготовлена с учетом его пожеланий и предпочтений. Верх модели выполнен из плотного текстиля. Этот материал отлично дышит. Система шнуровки лаконично вписывается в образ, а также плотно фиксирует стопу, что немаловажно во время игры. В области пятки имеется специальная петля, которая обеспечивает удобство при надевании. Внутри модели установлены мягкие вставки, которые придают комфорт во время использования. Стелька отлично дышит. Основную часть нагрузок берет на себя воздушный баллон, который расположен в области пятки. Резиновая подметка способна взаимодействовать с разными поверхностями. Также модель имеет кастомную расцветку.',1,3,'Текстиль',6890,0),(7,1,2,'Air Force 1 LV8 1 ','7.jpg','Кроссовки Nike Air Force 1 LV8 - одна из самых популярных моделей бренда. Верх модели изготовлен из искусственной кожи. Этот материал прост в уходе и универсален в использовании. Система шнуровки лаконично вписывается в образ модели. Внутри модели установлены мягкие вставки, которые придают дополнительный комфорт во время носки. Промежуточный материал подошва изготовлен с использованием технологии Air. Поэтому он способен погашать любые нагрузки. Рисунок резиновой подмётки разработан так, чтобы взаимодействие могло обеспечивается с любой поверхностью. По всему периметру имеется фирменные элементы бренда.',2,3,'Кожа ',6190,1),(8,1,2,'Air Max 720','8.jpg','Кроссовки Nike Ispa Air Max 720 - одна из самых футуристичных моделей бренда. В верхе модели имеется рельефное строение, изготовлен с использованием плотного текстиля и нейлоновой сетки. Сочетание этих материалов позволяет добиться максимальной универсальности. Система шнуровки лаконично вписывается в образ модели. В области пятки и на язычке располагаются специальные петли, которые обеспечивают дополнительный комфорт во время надевания. В основе промежуточного материала подошвы лежит воздушный баллон Air Max, погашающий различные нагрузки. Строение протектора разработано специально для данной модели. ',11,3,'Текстиль',6390,1),(9,1,1,'X9000l4','9.jpg','Спортивные кроссовки Adidas Х9000I4 Cyberpunk 2077 - кастомная модель классической пары Adidas Х9000I4. В строении верха используется нейлоновая сетка, которая способна пропускать через себя лишнее тепло. Система шнуровки лаконично вписывается в образ модели. Внутри модели установлены мягкие вставки, которые обеспечивают дополнительный комфорт во время ходьбы. Промежуточный материал подошвы выполнен с использованием материала Boost. Это популярная технология Adidas, которая обеспечивает отличную амортизацию. Подметка из резины способна взаимодействовать с разными поверхностями. Кастом модели заключается в уникальной расцветке, приуроченной к выходу игры Cyberpunk 2077. Сбоку имеется дизайнерская надпись. Модель сочетает в себе несколько цветов.',1,3,'Текстиль',6390,0),(15,1,2,'Air Max Speed','15.jpg','Кроссовки Nike Air Max Speed - кроссовки с классическим образом, но современными технологиями. Верх модели выполнен с использованием плотного текстиля и замши. Сочетание этих материалов позволяет добиться максимальной универсальности. Система шнуровки состоит из двух частей: стандартной шнуровки и липучки. В области пятки имеется специальная петля, которая обеспечивает удобство при надевании. Стелька отлично дышит. В основе промежуточного материала подошвы лежит воздушный баллон Air Max. Резиновая подметка способна взаимодействовать с разными поверхностями. Сбоку модели расположены фирменные декорирующие элементы. ',2,3,'Кожа',6690,0),(16,1,2,'Dunk SB','16.jpg','Кроссовки Nike Dunk SB выполнены с использованием искусственной кожи и замши. Сочетание этих материалов позволяет добиться элегантного образа и максимальной практичности. Система шнуровки лаконично вписывается в образ модели. За счёт то, что модель высокая обеспечивается плотная поддержка стопы. Внутри модели установлены мягкие вставки, которые придают комфорт во время ходьбы. Стелька обладает дышащими свойствами. Промежуточный материал подошвы изготавливаются по технологии Air, поэтому погашаются любые нагрузки. Резиновая подметка способна взаимодействовать с разными поверхностями. По периметру модель декорирована фирменными элементами. ',4,3,'Кожа',6390,0),(17,1,1,'Torsion X','17.jpg','Кроссовки Adidas Torsion X – комфортная спортивная обувь, которая пригодна и для повседневного использования. Верхняя часть изготовлена из комбинации инновационных материалов и нубука. Буква «Х» в названии модели обозначает, что в подошве имеет специальная вставка Х-образной формы, обеспечивает комфорт при движении и амортизацию стопы. Протектор подошвы имеет специальные овальные «пятна контакта». Шнуровка через специальные петли, расположенные на пяточной части кроссовок, обеспечивает более надежную фиксацию.',11,3,'Текстиль, замша',6090,0),(18,1,1,'EQT Bask ADV','18.jpg','Кроссовки Adidas EQT Bask Adv подойдут для занятий спортом. Верх модели выполнен с использованием нейлоновой сетки. Этот материал отлично пропускает лишнее тепло, позволяя комфортно использовать модель. Система шнуровки лаконично вписывается в образ модели. Внутри модели установлены мягкие вставки, которые обеспечивают дополнительный комфорт во время ходьбы. Промежуточный материал подошвы обладает отличными амортизационными свойствами. Он способен погашать любые нагрузки. За счёт уникальной технологии изготовления подмётки, она способна взаимодействовать с разными поверхностями. Рисунок также разработан для этой модели. По всему периметру модель декорирована фирменнымb элементами.',2,2,'Текстиль',5390,0),(19,1,3,'Classics Zig Kinetica','19.jpg','Кроссовки Reebok Classics Zig Kinetica - одна из самых футуристичных моделей бренда. Верх модели выполнен из плотного текстиля, который обладает рельефным строением. Использование этого материала придает универсальность в использовании и простоту в уходе. Система шнуровки лаконично вписывается в образ модели. Внутри модели установлены мягкие вставки, которые обеспечивают дополнительный комфорт во время ходьбы. Промежуточный материал подошвы выполнен по новейшим технологиям изготовления, что позволяет добиться максимальной амортизации. Резиновая подметка способна взаимодействовать с разными поверхностями. По периметру модели имеются фирменные логотипы бренда.',11,3,'Текстиль',6390,0),(21,1,1,'Marathon 2020','21.jpg','Кроссовки Adidas Marathon 2020 - модель, сочетающая в себе классический образ и современные технологии. В строении верха используется нейлоновая сетка, которая обладает отличными дышащими свойствами. Система шнуровки лаконично вписывается в образ модели. В области пятки установлена специальная петля, которая облегчает процесс надевания модели. Внутри модели имеются мягкие вставки, которые обеспечивают дополнительный комфорт во время ходьбы. Промежуточный материал подошвы обладает амортизационными свойствами, способными погашать любые нагрузки. Резиновая подметка способна взаимодействовать с разными поверхностями. По периметру модели имеются фирменные декорирующие элементы. ',2,3,'Кожа, текстиль',5390,0),(28,1,1,'Rivalry RM Low','28.jpg','Кроссовки Adidas Rivalry RM Low – обувь, которая изначально создавалась для занятия баскетболом в зале, но потом «вышла» на улицы. Слово «Low» в названии кроссовок подчеркивает их высоту, которая обеспечивает фиксацию голеностопа, вкупе с глубокой посадкой. Для большей долговечности верхняя часть кроссовок покрыта полиуретаном. В подошве имеются специальные капсулы, которые призваны обеспечить амортизацию стопы. Перфорация в районе плюсны и с боковой проекции, призвана обеспечить вентиляцию стопы. Фирменный брендинг в виде стилизованных трех полосок и эмблемы на язычке, завершает облик кроссовок. Протектор с разным рисунком обеспечивает надежное сцепление.',2,2,'Кожа',6690,0),(32,1,1,'Ozweego','32.jpg','Кроссовки Adidas Ozweego отлично подходят для занятий спортом. Верх модели обладает отличными дышащими свойствами. Он изготовлен из искусственной кожи и текстиля. Система шнуровки лаконично вписывается в образ модели. Внутри модели имеются мягкие вставки, которые обеспечивают дополнительный комфорт во время носки. Стелька обладает дышащими свойствами. Подошва имеет необычное рельефное строение. Промежуточный материал изготовлен с использованием новых технологий. Поэтому обеспечивается погашение любых нагрузок. Резиновая подметка отлично взаимодействует с разными поверхностями. По периметру модели имеются фирменные декорирующие элементы бренда.',2,3,'Кожа',6390,0);
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_goods`
--

DROP TABLE IF EXISTS `order_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_order` int NOT NULL,
  `id_good` int NOT NULL,
  `good_count` int NOT NULL,
  `good_size` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_goods`
--

LOCK TABLES `order_goods` WRITE;
/*!40000 ALTER TABLE `order_goods` DISABLE KEYS */;
INSERT INTO `order_goods` VALUES (1,1,2,1,'37'),(2,1,2,1,'38'),(3,2,1,1,'38'),(4,3,2,1,'39'),(5,3,2,1,'38'),(6,3,2,1,'40'),(7,4,3,1,'37'),(8,5,6,1,'37'),(9,6,2,1,'39'),(10,7,4,1,'37'),(11,8,1,1,'39'),(12,9,1,1,'38'),(13,10,4,1,'39'),(14,10,4,1,'38'),(15,11,20,1,'37'),(16,12,8,1,'37'),(17,13,7,1,'38'),(18,14,3,1,'41');
/*!40000 ALTER TABLE `order_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id_order` int NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `date_order` text NOT NULL,
  `status_order` varchar(100) NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (14,6,'17-11-2022','Обработан');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `season` (
  `id_season` int NOT NULL AUTO_INCREMENT,
  `season` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_season`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (1,'зима'),(2,'лето'),(3,'демисезон');
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  `login` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Админ','admin','3cf108a4e0a498347a5a75a792f2321221232f297a57a5a743894a0e4a801fc3','mail@mail.ru','admin'),(6,'Max','Max','332560cd4dc7b52f0e15e22d313160a681dc9bdb52d04dc20036dbd8313ed055','test@test.ru','user');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-17 23:16:01
