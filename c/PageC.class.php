<?php

/**
 * Контроллер показа страниц сайта
 */
class PageC extends BaseC {

    /**
     * Главная страница сайта '/' или '/index.php'
     */
    public function index() {

        $template = $this->twig -> loadTemplate('mainContent.twig');
        $this->content = $template -> render(array());
    }

    /**
     * Страница каталога '/catalog.php'
     */
    public function catalog() {
        $this->title .= ' | Каталог';

        $filters = $this->page -> filter(); //массив фильтров
        $goods = $this->page -> catalog('id', 'ASC', 24, $filters[0], $filters[1]); //массив товаров каталога

        $template = $this->twig -> loadTemplate('catalog.twig');
        $this->content = $template -> render(array('goods' => $goods));
    }

    /**
     * Страница описания товара '/good.php'
     */
    public function good() {
        $good = $this->page -> good(); //массив с описанием единицы товара

        $this->title .= ' | Каталог | ' . $good['title'];

        $template = $this->twig -> loadTemplate('good.twig');
        $this->content = $template -> render(array('good' => $good, 'title' => $this->title));
    }

}
